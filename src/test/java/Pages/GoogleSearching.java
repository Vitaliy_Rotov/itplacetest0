package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class GoogleSearching {

    private WebDriver driver;

    private static final String URL = "https://www.google.ru/";


    public GoogleSearching(final WebDriver driver, Class<GoogleSearching> googleSearchingClass) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@class=\"iUh30\"]")
    private WebElement firstHead;

    public void firstHeadCompare(String keyWord, String checkWord) {

        driver.get(URL);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

        WebElement searchBox = driver.findElement(By.name("q"));
        searchBox.sendKeys(keyWord);
        searchBox.submit();
        driver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS);

        String firstElement = firstHead.getText();

        if (firstElement.contains(checkWord)) {
            System.out.println("Ok");
        } else {
            throw new IllegalStateException("Wrong");
        }

    }

    public boolean firstHeadDisplayed() {
        return firstHead.isDisplayed();
    }

}
