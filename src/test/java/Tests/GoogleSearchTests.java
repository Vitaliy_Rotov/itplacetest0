package Tests;

import Pages.GoogleSearching;
import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleSearchTests {

    private static WebDriver driver;
    private static GoogleSearching googleSearching;

    private static final String KEY_WORD = "simbirsoft";
    private static final String CHECK_WORD = "https://www.simbirsoft.com/";


    @BeforeAll
    public static void setup() {

        System.setProperty( "webdriver.chrome.driver", "/Users/vitaliy_rotov/chromedriver" );
        driver = new ChromeDriver();
        googleSearching = new GoogleSearching( driver, GoogleSearching.class );

    }

    @Test
    public void testGoogleSearch() {

        try {
            googleSearching.firstHeadCompare(KEY_WORD, CHECK_WORD);
            Assert.assertTrue(googleSearching.firstHeadDisplayed());
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

    }

    @AfterAll
    public static void tearDown() {

        if (driver != null) {
            driver.quit();
        }

    }

}
